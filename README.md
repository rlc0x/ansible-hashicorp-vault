Ansible-hashicorp-vault
=========

:warning: **Not intended to be used in a production environment. The unseal method is not secure, and should be modified if using in production**


Ansible Playbook that manages a Hashicorp Vault Development instance behind a single HAProxy load balancer.

This playbook is meant to be run from an ansible control node. 



Tested With
------------

    - Rocky Linux 8.7
    - Proxmox VE 7.3
    - Ansible 2.9
    - Hashicorp Vault 1.13.0
    - FreeIPA 4.9.11

Requirements
------------

- Three nodes for Vault
  - Secondary disk for configuration of the **raft** storage backend
- One node for HAProxy

Playbook Variables
--------------

Required variables are listed below

**vault_primary_node**

The node on which `vault` will be initialized. The playbook expects to initialize `vault` on only one node.

**vault_leader_tls_servername**

Should be the HAProxy node

**vault_leader_api_addr**

Should be the HAProxy node

**vault_leader_client_cert_file**

Certificate used to secure communications with Vault. 

:note: **Create a certificate that has all four nodes and IP addresses as SAM entries**

**vault_leader_client_key_file**

The key file 

**vault_leader_ca_cert_file**

The CA certificate if using `FreeIPA` like me, this will be located in /etc/ipa/ca.crt

**haproxy_node_name**

Name of the node that will serve as the reverse proxy

**haproxy_bind_address**

IP address used for the proxy server

**haproxy_ssl_cert**

Chained certificate 

Dependencies
------------


Example Playbook
----------------

```yaml
---
- name: Install and configure Hashicorp Vault
  hosts: all
  become: True  
  tasks:
    - name: Configure the haproxy node
      include_role:
        name: vault-haproxy
      when: ansible_hostname == haproxy_node_name

    - name: Configure the vault nodes
      include_role:
        name: vault-dev-cluster
      when: ansible_hostname in groups['vault']
```

License
-------

MIT

Author Information
------------------

Raymond Cox

